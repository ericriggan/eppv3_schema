-- -----------------------------------------------------------------------------
-- CREATE SCHEMA
-- -----------------------------------------------------------------------------
IF NOT EXISTS (
    SELECT *
      FROM sys.schemas
     WHERE name = 'eppv3'
) EXEC (
    'CREATE SCHEMA [eppv3];'
);
GO

-- -----------------------------------------------------------------------------
-- CREATE TABLE plan_cycles
-- -----------------------------------------------------------------------------
IF OBJECT_ID ('[eppv3].[plan_cycles]', 'U') IS NOT NULL
DROP TABLE [eppv3].[plan_cycles];
GO

IF NOT EXISTS (
    SELECT *
      FROM INFORMATION_SCHEMA.TABLES
     WHERE TABLE_NAME = 'plan_cycles'
       AND TABLE_SCHEMA = 'eppv3'
)
CREATE TABLE [eppv3].[plan_cycles] (
                  id INT NOT NULL,
     plan_cycle_desc VARCHAR(50) NOT NULL,
    cycle_start_date DATE NOT NULL,
      cycle_end_date DATE NOT NULL,
-- PRIMARY KEY
    CONSTRAINT PK_plan_cycles PRIMARY KEY CLUSTERED (id),
-- INDEXES
    INDEX IX_plan_cycles_cycle_start_date NONCLUSTERED (plan_start_date),
    INDEX IX_plan_cycles_cycle_end_date NONCLUSTERED (plan_end_date)
);
GO

-- -----------------------------------------------------------------------------
-- CREATE TABLE branches
-- -----------------------------------------------------------------------------
IF OBJECT_ID ('[eppv3].[branches]', 'U') IS NOT NULL
DROP TABLE [eppv3].[branches];
GO

IF NOT EXISTS (
    SELECT *
      FROM INFORMATION_SCHEMA.TABLES
     WHERE TABLE_NAME = 'branches'
       AND TABLE_SCHEMA = 'eppv3'
)
CREATE TABLE [eppv3].[branches] (
    id CHAR(2) NOT NULL,
    branch_desc VARCHAR(30) NOT NULL,
    branch_abrv VARCHAR(3) NOT NULL,
-- PRIMARY KEY
    CONSTRAINT PK_branches PRIMARY KEY CLUSTERED (id),
-- INDEXES
    INDEX UQ_branches_branch_desc UNIQUE NONCLUSTERED (branch_name ASC),
    INDEX UQ_branches_branch_abrv UNIQUE NONCLUSTERED (branch_abrv ASC)
);
GO

-- -----------------------------------------------------------------------------
-- CREATE TABLE cost_centers
-- -----------------------------------------------------------------------------
IF OBJECT_ID ('[eppv3].[cost_centers]', 'U') IS NOT NULL
DROP TABLE [eppv3].[cost_centers];
GO

IF NOT EXISTS (
    SELECT *
      FROM INFORMATION_SCHEMA.TABLES
     WHERE TABLE_NAME = 'cost_centers'
       AND TABLE_SCHEMA = 'eppv3'
)
CREATE TABLE [eppv3].[cost_centers] (
    id CHAR(2) NOT NULL,
    cost_center_desc VARCHAR(31) NOT NULL,
-- PRIMARY KEY
    CONSTRAINT PK_cost_centers PRIMARY KEY (id),
-- INDEX
    INDEX UQ_cost_centers_cost_center_desc UNIQUE NONCLUSTERED (cost_center_desc ASC)
);
GO

-- -----------------------------------------------------------------------------
-- CREATE TABLE job_titles
-- -----------------------------------------------------------------------------
IF OBJECT_ID ('[eppv3].[job_titles]', 'U') IS NOT NULL
DROP TABLE [eppv3].[job_titles];
GO

IF NOT EXISTS (
    SELECT *
      FROM INFORMATION_SCHEMA.TABLES
     WHERE TABLE_NAME = 'job_titles'
       AND TABLE_SCHEMA = 'eppv3'
)
CREATE TABLE [eppv3].[job_titles] (
                id VARCHAR(15) NOT NULL,
    job_title_desc VARCHAR(50) NOT NULL,
-- PRIMARY KEY
    CONSTRAINT PK_job_titles PRIMARY KEY CLUSTERED (id),
-- INDEX
    INDEX UQ_job_titles_job_title_desc UNIQUE NONCLUSTERED (job_title_desc ASC)
);
GO

-- -----------------------------------------------------------------------------
-- CREATE TABLE employees
-- -----------------------------------------------------------------------------
IF OBJECT_ID ('[eppv3].[employees]', 'U') IS NOT NULL
DROP TABLE [eppv3].[employees];
GO

IF NOT EXISTS (
    SELECT *
      FROM INFORMATION_SCHEMA.TABLES
     WHERE TABLE_NAME = 'employees'
       AND TABLE_SCHEMA = 'eppv3'
)
CREATE TABLE [eppv3].[employees] (
                     id CHAR(8)      NOT NULL,
    reporting_branch_id CHAR(2)      NOT NULL, -- FK
         cost_center_id CHAR(2)      NOT NULL, --FK
           job_title_id VARCHAR(15)  NOT NULL, --FK
              full_name VARCHAR(71)  NOT NULL,
          supervisor_id CHAR(8)      NOT NULL,
         last_hire_date DATE         NULL,
          employee_type VARCHAR(35)  NOT NULL,
        employee_status VARCHAR(35)  NOT NULL,
        samaccount_name VARCHAR(256) NULL,
     physical_branch_id CHAR(2)      NOT NULL, -- FK
-- PRIMARY KEY
    CONSTRAINT PK_employees PRIMARY KEY CLUSTERED (id),
-- INDEXES
    INDEX IX_employees_reporting_branch_id NONCLUSTERED (reporting_branch_id ASC),
    INDEX IX_employees_cost_center_id      NONCLUSTERED (cost_center_id ASC),
    INDEX IX_employees_job_title_id        NONCLUSTERED (job_title_id ASC),
    INDEX IX_employees_physical_branch_id  NONCLUSTERED (physical_branch_id ASC),
    INDEX IX_employees_employee_type       NONCLUSTERED (employee_type ASC),
    INDEX IX_employees_employee_status     NONCLUSTERED (employee_status ASC),
-- FOREIGN KEYS
    CONSTRAINT FK_employees_reporting_branches
               FOREIGN KEY ([reporting_branch_id])
                REFERENCES [eppv3].[branches] ([id])
                 ON DELETE NO ACTION
                 ON UPDATE CASCADE,
    CONSTRAINT FK_employees_cost_centers
               FOREIGN KEY ([cost_center_id])
                REFERENCES [eppv3].[cost_centers] ([id])
                 ON DELETE NO ACTION
                 ON UPDATE CASCADE,
    CONSTRAINT FK_employees_job_titles
               FOREIGN KEY ([job_title_id])
                REFERENCES [eppv3].[job_titles] ([id])
                 ON DELETE NO ACTION
                 ON UPDATE CASCADE,
    CONSTRAINT FK_employees_physical_branches
               FOREIGN KEY ([physical_branch_id])
                REFERENCES [eppv3].[branches] ([id])
                 ON DELETE NO ACTION
                 ON UPDATE CASCADE
);
GO